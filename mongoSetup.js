//this file contains wrapper functions for intitializing MongoDB database connection
//as well as retrieving the database instance
//I am using the native mongodb module for node.js
const mongo = require('mongodb').MongoClient;
const config = require('./config');

var db;
var mongoclient;
var url = `mongodb://localhost:${config.MONGODB_PORT}/${config.DBNAME}`;

module.exports = {
	connect: function(callback){
		mongo.connect(url,{useNewUrlParser:true},function(err,client){
            if(err) return callback(err,null);
			db = client.db(config.DBNAME);
			mongoclient = client;
			return callback(null,db);
		});
	},
	getDb: function(){
		return db;
	},
	closeDb: function(){
		mongoclient.close();
	}
};