//this file contains the main business logic and logical operations of the API
const book = require('./BookModel').Book;
const assert = require('chai').assert;
const ObjectID = require("mongodb").ObjectID;

var exports = module.exports = {};

/*
    this function is invoked upon get all books request from routesAPI.js
    It executes a query to the database to retrieve all data from the books collection
    in the database
    returns a callback with null error and the result set upon successful operation
    returns a callback with an error and null result set if encountered an error during query
    execution
 */
exports.getBooks = function(db,callback){
    db.collection('books').find({}).toArray(function(err,result){
        if (err) return callback(err,null);
        callback(null,result);
    });
}
/*
    this function is invoked upon get book by ID request from routesAPI.js
    It executes a query to the database to retrieve the requested data from the books collection
    in the database
    returns a callback with null error and the result set upon successful operation
    returns a callback with "invalid parameter" error and null result set 
    if the ID supplied is of invalid format
    returns a callback with an error and null result set if encountered an error during query
    execution
 */
exports.getBook= function(db,id,callback){
    let objectID;
    try{    
        objectID = new ObjectID(id);
    }
    catch(e){
        //console.log(e);
        return callback("invalid parameter",null);
    }
    
    var idQuery = {"_id":objectID};
    db.collection('books').find(idQuery).toArray(function(err,result){
        try{
            assert.equal(null,err);
        }
        catch(e){
            console.log(e);
            return callback(err,null);
        }
        if(result.length === 0)return callback(null,result);
        var res = result[0];
        callback(null,res);
    });
}

/*
    this function is invoked upon book insertion request from routesAPI.js
    It executes a query to the database to push the requested data to the books collection
    in the database

    the supplied data must first be asserted with the expected data format before
    pushing them to the database

    returns a callback with null error and the result set upon successful operation
    returns a callback with "invalid JSON" error and null result set 
    if the supplied data is invalid
    returns a callback with "server JSON" error and null result set 
    if meets an unexpected error
    returns a callback with an error and null result set if encountered an error during query
    execution
 */
exports.postBook = function(db,data,callback){
    book.title = data.title;
    book.author = data.author;
    book.isbn = data.isbn;
    book.numberOfPages = data.numberOfPages;
    book.publishedOn = data.publishedOn;
    try{
        assert.typeOf(data.title,"string","expected title to be string");
        assert.typeOf(data.author,"string","expected author to be string");
        assert.typeOf(data.isbn,"string", "expected isbn to be string");
        assert.isNumber(data.numberOfPages,"expected numberOfPages to be Integer");
        assert.isNumber(data.publishedOn,"expected publishedOn to be Integer");
        assert.deepEqual(data,book);
    }
    catch(e){
        console.error(e);
        return callback("invalid JSON",null);

    }
    db.collection('books').insertOne(book,function(err,result){
        try{
            assert.equal(null,err);
        }
        catch(e){
            console.log(e);
            return callback(err,null);
        }
        callback(null,result);
    });
}

/*
    this function is invoked upon book update request from routesAPI.js
    It executes a query to the database to push the requested data to the books collection
    in the database
    
    the supplied data must first be asserted with the expected data format before
    pushing them to the database

    returns a callback with null error and the result set upon successful operation
    
    returns a callback with "invalid JSON" error and null result set 
    if the supplied data is invalid
    
    returns a callback with "invalid parameter" error and null result set 
    if the ID supplied is of invalid format
    
    returns a callback with "server error" error and null result set 
    if meets an unexpected error
    
    returns a callback with an error and null result set if encountered an error during query
    execution
 */
exports.updateBook = function(db,data,id,callback){
    delete book._id;
    book.title = data.title;
    book.author = data.author;
    book.isbn = data.isbn;
    book.numberOfPages = data.numberOfPages;
    book.publishedOn = data.publishedOn;
    try{
        assert.typeOf(data.title,"string","expected title to be string");
        assert.typeOf(data.author,"string","expected author to be string");
        assert.typeOf(data.isbn,"string", "expected isbn to be string");
        assert.isNumber(data.numberOfPages,"expected numberOfPages to be Integer");
        assert.isNumber(data.publishedOn,"expected publishedOn to be Integer");
        assert.deepEqual(data,book);
    }
    catch(e){
        console.error(e);
        return callback("invalid JSON",null);

    }
    let objectID;
    try{    
        objectID = new ObjectID(id);
    }
    catch(e){
        console.log(e);
        return callback("invalid parameter",null);
    }
    var idQuery = {_id:objectID};
    
    var updateQuery = {$set:book};
    db.collection('books').updateOne(idQuery,updateQuery,function(err,result){
        try{
            assert.equal(null,err);
        }
        catch(e){
            console.log(e);
            return callback(err,null);
        }

        if(result.matchedCount < 1){
            return callback(null,result);
        }
        callback(null,result);
    });
}
/*
    this function is invoked upon book deletion request from routesAPI.js
    It executes a query to the database to delete the requested book from the books collection
    in the database

    returns a callback with null error and the result set upon successful operation
    
    returns a callback with "invalid parameter" error and null result set 
    if the ID supplied is of invalid format
    
    returns a callback with an error and null result set if encountered an error during query
    execution
 */

exports.deleteBook = function(db,id,callback){
    let objectID;
    try{    
        objectID = new ObjectID(id);
    }
    catch(e){
        console.log(e);
        return callback("invalid parameter",null);
    }
    var idQuery = {_id:objectID}

    db.collection('books').deleteOne(idQuery,function(err,result){
        try{
            assert.equal(null,err);
        }
        catch(e){
            console.log(e);
            return callback(err,null);
        }
        if(result.deletedCount < 1)return callback(null,result);
        callback(null,result);
    });
}
