const client = require("./mongoSetup");//a MongoDB instance, used for direct database access
const book = require('./BookModel').Book;
const assert = require('chai').assert;
const ObjectID = require("mongodb").ObjectID;
const AssertionError = require('chai').AssertionError;

var exports = module.exports = {};

exports.getBooks = function(db,callback){
    db.collection('books').find({}).toArray(function(err,result){
        if (err) return callback(err,null);
        callback(null,result);
    });
}

exports.getBook= function(db,id,callback){
    let objectID;
    try{    
        objectID = new ObjectID(id);
    }
    catch(e){
        console.log(e);
        return callback("invalid parameter",null);
    }
    
    var idQuery = {"_id":objectID};console.log(id)
    db.collection('books').find(idQuery).toArray(function(err,result){
        if (err) return callback(err,null);
        var res = result[0];
        callback(null,res);
    });
}

exports.postBook = function(db,data,callback){
    console.log(data.title)
    book.title = data.title;
    book.author = data.author;
    book.isbn = data.isbn;
    book.numberOfPages = data.numberOfPages;
    book.publishedOn = data.publishedOn;
    try{
        assert.deepEqual(data,book);
        assert.typeOf(data.title,"string","expected title to be string");
        assert.typeOf(data.author,"string","expected author to be string");
        assert.typeOf(data.isbn,"string", "expected isbn to be string");
        assert.isNumber(data.numberOfPages,"expected numberOfPages to be Integer");
        assert.isNumber(data.publishedOn,"expected publishedOn to be Integer");
    }
    catch(e){
        if(e instanceof AssertionError){
            console.log(e)
            return callback("invalid JSON",null);
        }
        else{
            console.log(e)
            return callback("server Error",null);
        }
    }
    db.collection('books').insertOne(book,function(err,res){
         if(err) return callback(err,null);
        callback(null,res);
    });
}

exports.updateBook = function(db,data,id,callback){
    book.title = data.title;
    book.author = data.author;
    book.isbn = data.isbn;
    book.numberOfPages = data.numberOfPages;
    book.publishedOn = data.publishedOn;
    try{
        assert.deepEqual(data,book);
        assert.typeOf(data.title,"string","expected title to be string");
        assert.typeOf(data.author,"string","expected author to be string");
        assert.typeOf(data.isbn,"string", "expected isbn to be string");
        assert.isNumber(data.numberOfPages,"expected numberOfPages to be Integer");
        assert.isNumber(data.publishedOn,"expected publishedOn to be Integer");
    }
    catch(e){
        console.log(e);
        return callback("invalid JSON",null);
    }
    let objectID;
    try{    
        objectID = new ObjectID(id);
    }
    catch(e){
        console.log(e);
        return callback("invalid parameter",null);
    }
    var idQuery = {_id:objectID};
    
    var updateQuery = {$set:book};
    db.collection('books').updateOne(idQuery,updateQuery,function(err,res){
        if(err) return callback(err,null);
        callback(null,res);
    });
}
exports.deleteBook = function(db,id,callback){
    let objectID;
    try{    
        objectID = new ObjectID(id);
    }
    catch(e){
        console.log(e);
        return callback("invalid parameter",null);
    }
    var idQuery = {_id:objectID}

    db.collection('books').deleteOne(idQuery,function(err,result){
        if(err) return callback(err,null);
        console.log(result.deletedCount);
        callback(null,result);
    });
}
