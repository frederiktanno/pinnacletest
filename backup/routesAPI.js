//this file contains the main REST API service functionalities
//it returns the Router object, which will be used in index.js

const router = require('express').Router();
const mongo = require("./mongoSetup");
const restServices = require('./restServices');
const config = require("./config");

mongo.connect(function(err,db){
    if(err){
        console.log(`error connecting to database: ${err}`);
        return;
    }//console.log(result);
    console.log(`successfully connected to mongoDB at port ${config.MONGODB_PORT}`);

    router.get("/books",(req,res,next)=>{
       restServices.getBooks(db,function(err,result){
            if(err){
                return res.status(500).send("Server Error");
            }
            return res.json(result);
         })
    });

    router.get("/books/:id",(req,res,next)=>{
        var id = req.params.id;
        if(!id){
            next();
        }
        restServices.getBook(db,id,function(err,result){
            if(err == "invalid parameter"){
                //in compliance with RFC 7231
                return res.status(400).send("Invalid Parameter in the URL");
            }
            else{
                if(err){
                    console.log(err);
                    return res.status(500).send("Server error");
                }
                if(result==null){
                    return res.status(404).send("Book Not Found");
                    
                }
            }
            
            return res.json(result);
            
         })
    });

    router.post("/books",(req,res)=>{
        var data = req.body;
        restServices.postBook(db,data,function(err,result){
            if(err == "invalid JSON"){
                console.log(err);
                return res.status(400).send("Invalid JSON Format");
            }
            if(err == "server Error"){
                return res.status(500).send("Unexpected Error");
            }
            else{
                return res.status(200).send("New Book Added");
            }
        });
    });

    router.put("/books/:id",(req,res,next)=>{
        var data = req.body;
        var id = req.params.id;
        if(!id){
            console.log("here");
            next();
        }
        restServices.updateBook(db,data,id,function(err,result){
            if(err=="invalid JSON"){
                return res.status(400).send("Invalid JSON Format");
            }
            if(err == "invalid parameter"){
                return res.status(400).send("Invalid Parameter in the URL");
            }
            else{
                if(result.matchedCount < 1){
                    return res.status(404).send("Book Not Found");
                }
                if(err){
                    console.log(err);
                    return res.status(500).send("Server Error");
                }
                else{
                    return res.status(200).send("Book Updated");
                }
                
            }
        });
    });

    router.delete("/books/:id",(req,res,next)=>{
        var id = req.params.id;
        if(!id){
            console.log("here");
            next();
        }
        restServices.deleteBook(db,id,function(err,result){
            if(err == "invalid parameter"){
                return res.status(400).send("Invalid Parameter in the URL");
            }
            else{
                if(err){
                    console.log(err);
                    return res.status(500).send("Server Error");
                }
                if(result.deletedCount < 1){
                    return res.status(404).send("Book Not Found");
                }
                else{
                    return res.status(200).send("Book Deleted");
                }
            }
        });
            
    });
});

module.exports = {
    router
}