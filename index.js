/*
  Author: Laurensius Frederik Tanno
  Note: this application uses 2 databases, whose files are supplied in the directory
  as db.json and db_test.json
  Pinnacle_books.json is the main database used for the actual REST API
  Pinnacle_test_books.json is the test database used for unit testing
  the collection used for both databases is called 'books'
 */
const app = require("express")();
const config = require("./config");
const port = config.PORT;
const routesAPI = require("./routesAPI");
const bodyParser = require("body-parser");

app.use(bodyParser.json());

app.listen(port, () =>
  console.log(`Server listening on port ${port}!`)
);
//uses the router middleware
const router = routesAPI.router;
app.use("/pinnacle/",router);
