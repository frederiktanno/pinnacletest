//this file contains the main REST API service functionalities
//acts as the router middleware to be used in index.js

const router = require('express').Router();
const mongo = require("./mongoSetup");
const restServices = require('./restServices');
const config = require("./config");

//the router needs to be encapsulated in the mongo.connect callback, to keep a reference of the database instance
mongo.connect(function(err,db){
    if(err){
        console.log(`error connecting to database: ${err}`);
        return;
    }//console.log(result);
    console.log(`successfully connected to mongoDB at port ${config.MONGODB_PORT}`);

    /*
        GET http://localhost:${config.PORT}/pinnacle/books
        This route returns an HTTP status 200 and a JSON representation of all books in the database
        upon successful operation, or error 500 with a message "Server Error" in case of unexpected database error
     */
    router.get("/books",(req,res,next)=>{
       restServices.getBooks(db,function(err,result){
            if(err){
                return res.status(500).send("Server Error");
            }
            return res.json(result);
         })
    });

    /*
        GET http://localhost:${config.PORT}/pinnacle/books/:id
        The expected parameter is the correct representation of a book ID (the ID that is generated as primary key by MongoDB)
        This route returns an HTTP status 200 and a JSON representation of the book with the given ID in the database
        upon successful operation,
        HTTP status 400 and message of "Invalid Parameter in the URL" in case of invalid ID type
        (i.e some random string or numbers)
        HTTP status 404 and message of "Book not found" in case of valid ID format but the database returns
        an empty result set
        HTTP status 500 with a message "Server Error" in case of unexpected database error
     */
    router.get("/books/:id",(req,res,next)=>{
        var id = req.params.id;
        if(!id){
            next();
        }
        restServices.getBook(db,id,function(err,result){
            if(err == "invalid parameter"){                
                return res.status(400).send("Invalid Parameter in the URL");
            }
            else{
                if(err){
                    console.log(err);
                    return res.status(500).send("Server error");
                }
                if(result==null || result.length === 0){
                    //in compliance with RFC 7231
                    return res.status(404).send("Book Not Found");
                    
                }
            }
            
            return res.json(result);
            
         })
    });

    /*
        POST http://localhost:${config.PORT}/pinnacle/books/
        This route returns an HTTP status 200 and a JSON object containing the ID of the inserted data
        upon successful operation,
        HTTP status 400 and message of "Invalid JSON Format" in case of misshapen JSON given in the request body
        expected format:
        {
            "title":string,
            "author":string,
            "isbn":string,
            "publishedOn":integer,
            "numberOfPages":integer
        }
        HTTP status 500 with a message "Server Error" in case of unexpected database error
     */
    router.post("/books",(req,res)=>{
        var data = req.body;
        restServices.postBook(db,data,function(err,result){
            if(err == "invalid JSON"){
                console.log(err);
                return res.status(400).send("Invalid JSON Format");
            }
            if(err == "server Error"){
                return res.status(500).send("Unexpected Error");
            }
            else{
                return res.status(200).json({id:result.insertedId});
            }
        });
    });

    /*
        PUT http://localhost:${config.PORT}/pinnacle/books/:id
        This route returns an HTTP status 200 and a message saying "Book Updated"
        upon successful operation,
        HTTP status 400 and message of "Invalid JSON Format" in case of misshapen JSON given in the request body
        expected format:
        {
            "title":string,
            "author":string,
            "isbn":string,
            "publishedOn":integer,
            "numberOfPages":integer
        }
        HTTP status 400 and message of "Invalid Parameter in the URL" in case of invalid ID type
        (i.e some random string or numbers)
        HTTP status 404 and message of "Book not found" in case of valid ID format but the database returns
        an empty result set
        HTTP status 500 with a message "Server Error" in case of unexpected database error
     */
    router.put("/books/:id",(req,res,next)=>{
        var data = req.body;
        var id = req.params.id;
        if(!id){
            console.log("here");
            next();
        }
        restServices.updateBook(db,data,id,function(err,result){
            if(err=="invalid JSON"){
                return res.status(400).send("Invalid JSON Format");
            }
            if(err == "invalid parameter"){
                return res.status(400).send("Invalid Parameter in the URL");
            }
            else{
                if(result == null){
                    return res.status(404).send("Book Not Found");
                }
                if(err){
                    console.log(err);
                    return res.status(500).send("Server Error");
                }
                else{
                    return res.status(200).send("Book Updated");
                }
                
            }
        });
    });

    /*
        DELETE http://localhost:${config.PORT}/pinnacle/books/:id
        The expected parameter is the correct representation of a book ID (the ID that is generated as primary key by MongoDB)
        This route returns an HTTP status 200 and a message of "Book Deleted"
        upon successful operation,
        HTTP status 400 and message of "Invalid Parameter in the URL" in case of invalid ID type
        (i.e some random string or numbers)
        HTTP status 404 and message of "Book not found" in case of valid ID format but the database returns
        an empty result set
        HTTP status 500 with a message "Server Error" in case of unexpected database error
     */
    router.delete("/books/:id",(req,res,next)=>{
        var id = req.params.id;
        if(!id){
            console.log("here");
            next();
        }
        restServices.deleteBook(db,id,function(err,result){
            if(err == "invalid parameter"){
                return res.status(400).send("Invalid Parameter in the URL");
            }
            else{
                if(err){
                    console.log(err);
                    return res.status(500).send("Server Error");
                }
                if(result == null){
                    return res.status(404).send("Book Not Found");
                }
                else{
                    return res.status(200).send("Book Deleted");
                }
            }
        });
            
    });
});

module.exports = {
    router
}