const expect = require('chai').expect;
const config = require('../config');
const MongoClient = require('mongodb').MongoClient;
const restServices = require('../restServices');

let db;
let mongoclient;
var url = `mongodb://localhost:${config.MONGODB_PORT}/${config.DBNAME_TEST}`;
let mongoID;//store the generated ID during successful book post test, which in turn will also be
//used for successful book deletion test 

describe("restServicesTest",function(){
    //before running tests, connect to the testing database
    before("connecting to test database",done =>{
        MongoClient.connect(url,{useNewUrlParser:true},function(err,client){
            db = client.db(config.DBNAME_TEST);
            mongoclient = client;
            if(err){
                console.log(err);
                done(err);
            }
            console.log("database connected successfully");
            done();
			
		})
    })

    describe("get all books",function(){
        it("should return all books with no errors",function(done){
            restServices.getBooks(db, function(err,result){
                if(err){
                    console.log(err);
                    return done(err);
                }
                expect(result).to.be.ok;
                //console.log(result);
                done();
            });
        });
    });

    describe("successful retrieving book by id",function(){
        it("should return the correct book",function(done){
            var id ="5d3f0a0872ebfd3784b86696";
            restServices.getBook(db, id,function(err,result){
                if(err){
                    console.log(err);
                    return done(err);
                }
                expect(result).to.be.ok;
                //console.log(result);
                done();
            });
        });
    });
    describe("failed retrieving book by id",function(){
        
        it("In case of valid ID but no results found, return result of length 0",function(done){
            var id ="5d3f0a0872ebfd3784b86680";            
            restServices.getBook(db, id,function(err,result){
                if(err){
                    //console.log(err);
                    return done(err);
                }
                else{
                    expect(result).to.have.lengthOf(0);                   
                    done();
                }              
            });
        });

        it("In case of invalid ID, return invalid parameter error",function(done){
            var id = "223";
            restServices.getBook(db, id,function(err,result){
                if(err){
                    expect(result).to.be.null;
                    //console.log(err);
                    return done();
                }
                else{
                    done(err);
                }              
            });
        });
    });
    describe("successful book insertion",function(){
        it("should return null error and non-null result",function(done){
            var book = {
                "title":"Assassin's Creed Renaissance",
	            "author":"Oliver Bowden",
	            "isbn":"978-602-8801-21-8",
	            "publishedOn":2012,
	            "numberOfPages":591
            };
            restServices.postBook(db,book,function(err,result){
                if(err)return done(err);
                expect(result).to.be.ok;
                mongoID = result.insertedId;

                done();
                });
            });
        });
        describe("failed book insertion",function(){
            it("should return invalid JSON format error",function(done){
                var book = {
                    "title":"Assassin's Creed Renaissance",
                    "author":"Oliver Bowden",
                    "isbn":978-602-8801-21-8,
                    "published":"2012",
                    "numberOfPages":591
                };
                restServices.postBook(db,book,function(err,result){
                    if(err){
                        expect(result).to.be.null;
                        //console.log(err);
                        return done();
                    }
                    else{
                        done(err);
                    }  
                });
            });
        });
        describe("successful book update",function(){
            
            it("should return null error",function(done){
                var book = {
                    "title":"Assassin's Creed Brotherhood",
                    "author":"Oliver Bowden",
                    "isbn":"978-602-9159-46-2",
                    "publishedOn":2011,
                    "numberOfPages":600
                };
                var id = "5d41bef2eb77fd0c50567fb2";
                restServices.updateBook(db,book,id,function(err,result){
                    if(err){
                        console.log(err);
                        return done(err);
                    }
                    expect(result).to.be.ok;
                    //console.log(result);
                    done();
                    });
            });
        });

        describe("failure book update",function(){

            it("For invalid ID, should return invalid parameter error",function(done){
                var book = {
                    "title":"Assassin's Creed Renaissance",
                    "author":"Oliver Bowden",
                    "isbn":"978-602-8801-21-8",
                    "publishedOn":2012,
                    "numberOfPages":591
                };
                var id = "2345";
                restServices.updateBook(db,book,id,function(err,result){
                    if(err){
                        expect(result).to.be.null;
                        //console.log(err);
                        return done();
                    }
                    else{
                        done(err);
                    }  
                });
            });

            it("For invalid JSON format, should return invalid JSON format error",function(done){
                var book = {
                    "title":"Assassin's Creed Renaissance",
                    "author":"Oliver Bowden",
                    "isbn":978-602-8801-21-8,
                    "published":"2012",
                    "numberOfPages":591
                };
                var id = "5d41bef2eb77fd0c50567fb2";
                restServices.updateBook(db,book,id,function(err,result){
                    if(err){
                        expect(result).to.be.null;
                        //console.log(err);
                        return done();
                    }
                    else{
                        done(err);
                    }  
                });
            });

            it("For book not found, result contains matchedCount < 1",function(done){
                var book = {
                    "title":"Assassin's Creed Brotherhood",
                    "author":"Oliver Bowden",
                    "isbn":"978-602-9159-46-2",
                    "publishedOn":2011,
                    "numberOfPages":600
                };
                var id = "5d41bef2eb77fd0c50567fc3";
                restServices.updateBook(db,book,id,function(err,result){
                    if(err){
                        console.log(err);
                        return done(err);
                    }
                    expect(result.matchedCount).to.be.below(1);
                    //console.log(result);
                    done();
                    });
            });
            
        });
        describe("successful book deletion",function(){
            
            it("should return null error",function(done){
                var id = mongoID;
                restServices.deleteBook(db,id,function(err,result){
                    if(err){
                        console.log(err);
                        return done(err);
                    }
                    expect(result).to.be.ok;
                    //console.log(result);
                    done();
                    });
            });
        });

        describe("failed book deletion",function(){
        
            it("In case of valid ID but no results found, return result.deletedCount < 1",function(done){
                var id ="5d3f0a0872ebfd3784b86680";            
                restServices.deleteBook(db, id,function(err,result){
                    if(err){
                        console.log(err);
                        return done(err);
                    }
                    else{
                        expect(result.deletedCount).to.be.below(1);                   
                        done();
                    }              
                });
            });
    
            it("In case of invalid ID, return invalid parameter error",function(done){
                var id = "223";
                restServices.deleteBook(db, id,function(err,result){
                    if(err){
                        expect(result).to.be.null;
                        //console.log(err);
                        return done();
                    }
                    else{
                        done(err);
                    }              
                });
            });
        });

        after("closing database",function(){
            mongoclient.close();
        });
    });


